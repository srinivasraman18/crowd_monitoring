from django.db import models

# Create your models here.


class Events(models.Model):
	name = models.CharField(max_length=100)
	count = models.IntegerField(default=0)


class Hospitality(models.Model):
	college_name = models.CharField(max_length=100)
	count = models.IntegerField(default=0)

class EventCheckin(models.Model):
	event_id = models.ForeignKey(Events,on_delete=models.CASCADE)
	checkin_time = models.DateTimeField(auto_now_add = True)

class HospitalityCheckin(models.Model):
	college_id = models.ForeignKey(Hospitality,on_delete = models.CASCADE)
	checkin_time = models.DateTimeField(auto_now_add = True)