from channels.generic.websocket import WebsocketConsumer
import json
from asgiref.sync import async_to_sync

class DataConsumer(WebsocketConsumer):

	def connect(self):
		
		async_to_sync(self.channel_layer.group_add)('events',self.channel_name)
		self.accept()
		self.send(json.dumps({'text':'websocket connected'}))

	def disconnect(self,close_code):

		async_to_sync(self.channel_layer.group_discard)
		(
		'events',self.channel_name
		)
		

	def socket_send(self, data):
		self.send(data['text'])

	def receive(self, text_data):
		text_data_json = json.loads(text_data)
		message = text_data_json['text']
		sef.send(text_data = json.dumps({'text':message}))

