from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
import json
from api.models import Events, Hospitality
from django.db.models import F

# Create your views here.

permission_classes = (IsAuthenticated,) 
@api_view(['POST',])
def hospitality_checkin(request):
	data_dict = dict(request.data)
	college_id = data_dict['id']
	Hospitality.objects.filter(id=college_id).update(count= F('count')+1)
	updated_obj = Hospitality.objects.get(pk=college_id)
	updated_obj.refresh_from_db()
	top_colleges = Hospitality.objects.order_by('-count')
	top_colleges = top_colleges[:10]
	college_count = {}
	for college in top_colleges:
		college_count[college.college_name] = college.count
	send_data = {}
	send_data['data'] = college_count
	send_data['type'] = 'hospitality'
	channel_layer = get_channel_layer()
	async_to_sync(channel_layer.group_send)("events",{"type":"socket_send","text":json.dumps(send_data)})
	return Response(status=status.HTTP_201_CREATED)

permission_classes = (IsAuthenticated,) 
@api_view(['POST',])
def event_checkin(request):
	data_dict = dict(request.data)
	event_id = data_dict['id']
	print(event_id)
	Events.objects.filter(id=event_id).update(count= F('count')+1)
	updated_obj = Events.objects.get(pk=event_id)
	updated_obj.refresh_from_db()
	top_events = Events.objects.order_by('-count')
	top_events = top_events[:10]
	event_count = {}
	for event in top_events:
		event_count[event.name] = event.count
	send_data = {}
	send_data['id'] = event_id
	send_data['data'] = event_count
	send_data['type'] = 'events'
	channel_layer = get_channel_layer()
	async_to_sync(channel_layer.group_send)("events",{"type":"socket_send","text":json.dumps(send_data)})
	return Response(status=status.HTTP_201_CREATED)


permission_classes = (IsAuthenticated,) 
@api_view(['POST',])
def sentiment_analysis(request):
	pass


def visualisation_view(request):
	return render(request,'visualisations.html')